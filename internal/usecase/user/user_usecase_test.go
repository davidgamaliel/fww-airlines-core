package user

import (
	"testing"
	"time"

	"github.com/fww/core/config"
	userModel "github.com/fww/core/internal/model/user"
	mock_config "github.com/fww/core/mock/config"
	mock_repository "github.com/fww/core/mock/repository"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestUserUsecaseLogin(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	loggerMock := mock_config.NewMockLogger(ctrl)
	baseMock := config.BaseDep{
		Logger: loggerMock,
	}

	repoMock := mock_repository.NewMockUserPersister(ctrl)
	usecaseMock := NewUserExecutor(UserUsecase{
		BaseDep:    &baseMock,
		Repository: repoMock,
		Secret:     "",
	})

	param := userModel.UserCredential{
		Email:    "ucok@ucok.com",
		Password: "pass123",
	}

	today := time.Now()
	res := userModel.User{
		ID:        1,
		Email:     "ucok@ucok.com",
		Name:      "Ucok",
		PhoneNo:   "6211111",
		CreatedAT: &today,
		UpdatedAT: &today,
	}
	t.Run("success get user credentials", func(t *testing.T) {

		repoMock.EXPECT().FindByCredential(param).Return(res, nil)
		res, err := usecaseMock.Login(param)

		assert.Nil(t, err)
		assert.NotNil(t, res)
	})
}
