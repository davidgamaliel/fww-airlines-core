package user

import (
	"time"

	"github.com/fww/core/config"
	"github.com/fww/core/internal/helper"
	"github.com/fww/core/internal/model"
	userModel "github.com/fww/core/internal/model/user"
	userRepo "github.com/fww/core/internal/repository/user"
	jtoken "github.com/golang-jwt/jwt/v4"
)

type UserUsecase struct {
	BaseDep    *config.BaseDep
	Repository userRepo.UserPersister
	Secret     string
}

type UserExecutor interface {
	Login(req userModel.UserCredential) (string, *model.BusinessError)
}

func NewUserExecutor(u UserUsecase) UserExecutor {
	return &u
}

func (u *UserUsecase) Login(req userModel.UserCredential) (string, *model.BusinessError) {
	res, err := u.Repository.FindByCredential(req)
	if err != nil {
		return "", &model.BusinessError{
			Code:    helper.ERROR_BASE_CODE,
			Message: helper.ERROR_BASE_MSG,
		}
	}

	day := time.Hour * 24
	// Create the JWT claims, which includes the user ID and expiry time
	claims := jtoken.MapClaims{
		"ID":    res.ID,
		"email": res.Email,
		"name":  res.Name,
		"exp":   time.Now().Add(day * 1).Unix(),
	}
	// Create token
	token := jtoken.NewWithClaims(jtoken.SigningMethodHS256, claims)
	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte(u.Secret))
	if err != nil {
		return "", &model.BusinessError{
			Code:    helper.ERROR_BASE_CODE,
			Message: "secret not valid",
		}
	}

	return t, nil
}
