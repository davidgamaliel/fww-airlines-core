package flight

import (
	"github.com/fww/core/config"
	"github.com/fww/core/internal/helper"
	baseModel "github.com/fww/core/internal/model"
	model "github.com/fww/core/internal/model/flight"
	repository "github.com/fww/core/internal/repository/flight"
)

type FlightUsecase struct {
	BaseDep    *config.BaseDep
	Repository repository.FlightPersister
}

type FlightExecutor interface {
	FindFlights() ([]model.FlightLite, int64, *baseModel.BusinessError)
}

func NewFlightExecutor(r FlightUsecase) FlightExecutor {
	return &r
}

func (u *FlightUsecase) FindFlights() ([]model.FlightLite, int64, *baseModel.BusinessError) {
	var total int64
	fligths, err := u.Repository.FindFlights()
	if err != nil {
		return fligths, total, &baseModel.BusinessError{
			Code:    helper.ERROR_BASE_CODE,
			Message: helper.ERROR_BASE_MSG,
		}
	}

	return fligths, total, nil
}
