package airport

import (
	"github.com/fww/core/config"
	"github.com/fww/core/internal/helper"
	baseModel "github.com/fww/core/internal/model"
	model "github.com/fww/core/internal/model/airport"
	repository "github.com/fww/core/internal/repository/airport"
)

type AirportUsecase struct {
	BaseDep    *config.BaseDep
	Repository repository.AirportPersister
}

type AirportExecutor interface {
	FindAirports(req model.FindAirportsRequest) ([]model.Airport, int64, *baseModel.BusinessError)
}

func NewAirportExecutor(r AirportUsecase) AirportExecutor {
	return &r
}

func (u *AirportUsecase) FindAirports(req model.FindAirportsRequest) ([]model.Airport, int64, *baseModel.BusinessError) {
	fligths, total, err := u.Repository.FindAirports(req)
	if err != nil {
		return fligths, total, &baseModel.BusinessError{
			Code:    helper.ERROR_BASE_CODE,
			Message: helper.ERROR_BASE_MSG,
		}
	}

	return fligths, total, nil
}
