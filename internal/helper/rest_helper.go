package helper

import (
	"net/http"

	"github.com/fww/core/internal/model"
	cred "github.com/fww/core/internal/model/user"
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	jtoken "github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
)

func SuccessResponse(code int, data interface{}) model.Response {
	return model.Response{
		Data: data,
		Meta: model.Meta{
			Code:    code,
			Message: SUCCESS_RESPONSE_MSG,
		},
	}
}

func SuccessResponseWithoutData(code int) model.ResponseWithoutData {
	return model.ResponseWithoutData{
		Meta: model.Meta{
			Code:    code,
			Message: SUCCESS_RESPONSE_MSG,
		},
	}
}

func BusinessErrorResponse(err *model.BusinessError) model.Response {
	code := DEFAULT_BUSINESS_ERROR_CODE
	message := DEFAULT_BUSINESS_ERROR_MESSAGE
	if err != nil {
		code = err.Code
		message = err.Message
	}
	return model.Response{
		Data: nil,
		Meta: model.Meta{
			Code:    code,
			Message: message,
		},
	}
}

func ValidatorErrorResponse(err []*model.ErrorFieldResponse) model.Response {
	return model.Response{
		Data: nil,
		Meta: model.Meta{
			Code:    ERROR_INVALID_PARAM_CODE,
			Message: ERROR_INVALID_PARAM_MSG,
		},
		Errors: err,
	}
}

func ValidateStruct(err error) []*model.ErrorFieldResponse {
	var errors []*model.ErrorFieldResponse
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			var element model.ErrorFieldResponse
			element.Field = err.Field()
			element.ErrMessage = err.Tag()
			errors = append(errors, &element)
		}
	}
	return errors
}

func AbortUnauthorized(ctx *fiber.Ctx) error {
	return ctx.Status(http.StatusUnauthorized).JSON(model.Response{
		Data: nil,
		Meta: model.Meta{
			Code:    ERROR_UNAUTHORIZE_CODE,
			Message: ERROR_UNAUTHORIZE_MSG,
		},
	})
}

func GetIdentity(c *fiber.Ctx) cred.UserIdentity {
	user := c.Locals("user").(*jtoken.Token)
	claims := user.Claims.(jtoken.MapClaims)
	return cred.UserIdentity{
		ID:           int64(claims["ID"].(float64)),
		Name:         claims["name"].(string),
		Email:        claims["email"].(string),
		RequestID:    c.Get("x-request-id", uuid.NewString()),
		CacheControl: c.Get("x-cache-control", ""),
	}
}
