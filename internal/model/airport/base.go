package airport

import (
	"github.com/fww/core/internal/model"
)

type Airport struct {
	ID       int64           `json:"id" db:"id"`
	Name     string          `json:"name" db:"name"`
	Code     string          `json:"code" db:"code"`
	Country  string          `json:"country" db:"country"`
	Province string          `json:"province" db:"province"`
	City     string          `json:"city" db:"city"`
	District string          `json:"district" db:"district"`
	Area     string          `json:"area" db:"area"`
	Slug     model.JSONB_ARR `json:"slug" db:"slug" gorm:"type:jsonb"`
}

func (Airport) TableName() string {
	return "airports"
}
