package airport

type FindAirportsRequest struct {
	Limit int    `json:"limit" query:"limit"`
	IDLT  int    `json:"id_lt" query:"id_lt"`
	Slug  string `json:"slug" query:"slug"`
}
