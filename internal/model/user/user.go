package user

import "time"

type User struct {
	ID        int64      `db:"id"`
	Name      string     `db:"name"`
	Email     string     `db:"email"`
	Password  string     `db:"password"`
	PhoneNo   string     `db:"phone_no"`
	CreatedAT *time.Time `db:"created_at"`
	UpdatedAT *time.Time `db:"updated_at"`
}

func (User) TableName() string {
	return "users"
}
