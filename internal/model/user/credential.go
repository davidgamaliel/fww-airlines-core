package user

type UserCredential struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type UserIdentity struct {
	ID           int64
	Name         string
	Email        string
	RequestID    string
	CacheControl string
}
