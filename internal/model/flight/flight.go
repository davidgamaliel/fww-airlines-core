package flight

import (
	"time"

	"github.com/fww/core/internal/model"
	"github.com/fww/core/internal/model/airport"
)

type Flight struct {
	ID             int64           `json:"id" db:"id"`
	Code           string          `json:"code" db:"code"`
	PlaneID        int64           `json:"plane_id" db:"plane_id"`
	OriginID       int64           `json:"origin_id" db:"origin_id"`
	DestinationID  int64           `json:"destination_id" db:"destination_id"`
	AvailableSeats int64           `json:"available_seats" db:"available_seats"`
	OnHandSeats    int64           `json:"-" db:"on_hand_seats"`
	Reservedseats  int64           `json:"-" db:"reserved_seats"`
	Price          float64         `json:"price" db:"price"`
	DepartAt       time.Time       `json:"depart_at" db:"depart_at"`
	Duration       int64           `json:"duration" db:"duration"`
	Slug           model.JSONB_ARR `json:"slug" db:"slug" gorm:"type:jsonb"`
	Origin         airport.Airport `json:"origin" gorm:"foreignKey:OriginID"`
	Destination    airport.Airport `json:"destination" gorm:"foreignKey:DestinationID"`
}

func (Flight) TableName() string {
	return "flights"
}

type FlightLite struct {
	ID              int64     `json:"id" db:"id"`
	Code            string    `json:"code" db:"code"`
	PlaneID         int64     `json:"plane_id" db:"plane_id"`
	OriginID        int64     `json:"origin_id" db:"origin_id"`
	OriginName      string    `json:"origin_name" db:"origin_name"`
	OriginCode      string    `json:"origin_code" db:"origin_code"`
	DestinationID   int64     `json:"destination_id" db:"destination_id"`
	DestinationName string    `json:"destination_name" db:"destination_name"`
	DestinationCode string    `json:"destination_code" db:"destination_code"`
	AvailableSeats  int64     `json:"available_seats" db:"available_seats"`
	OnHandSeats     int64     `json:"on_hand_seats" db:"on_hand_seats"`
	Reservedseats   int64     `json:"reserved_seats" db:"reserved_seats"`
	Price           float64   `json:"price" db:"price"`
	DepartAt        time.Time `json:"depart_at" db:"depart_at"`
	Duration        int64     `json:"duration" db:"duration"`
}

func (FlightLite) TableName() string {
	return "flights"
}
