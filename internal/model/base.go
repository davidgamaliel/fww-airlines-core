package model

import (
	"encoding/json"
	"errors"
)

// type JSONB map[string]interface{}
type JSONB_ARR []string

// Value Marshal
func (jsonField JSONB_ARR) Value() ([]string, error) {
	if len(jsonField) == 0 {
		return nil, nil
	}
	return jsonField, nil
}

// Scan Unmarshal
func (jsonField *JSONB_ARR) Scan(value interface{}) error {
	data, ok := value.([]byte)
	if !ok {
		return errors.New("type assertion to []byte failed")
	}
	return json.Unmarshal(data, &jsonField)
}
