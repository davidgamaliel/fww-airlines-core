package user

import (
	"crypto/md5"
	"fmt"

	"github.com/fww/core/config"
	"github.com/fww/core/internal/model/user"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

type UserRepository struct {
	BaseDep *config.BaseDep
	Pool    *gorm.DB
}

type UserPersister interface {
	FindByCredential(req user.UserCredential) (user.User, error)
}

func NewUserPersister(r UserRepository) UserPersister {
	return &r
}

func (r *UserRepository) FindByCredential(req user.UserCredential) (user.User, error) {
	var acc user.User

	encrypted := fmt.Sprintf("%x", md5.Sum([]byte(req.Password)))
	fmt.Println("PASSWORD d41d8cd98f00b204e9800998ecf8427e >> ", encrypted)

	err := r.Pool.Where(&user.User{
		Email:    req.Email,
		Password: encrypted,
	}).First(&acc).Error
	if err != nil {
		r.BaseDep.Logger.Error("failed get user by credential", zap.Error(err))
		return acc, err
	}

	return acc, nil
}
