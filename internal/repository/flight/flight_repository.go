package flight

import (
	"github.com/fww/core/config"
	flightModel "github.com/fww/core/internal/model/flight"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

type FlightRepository struct {
	BaseDep *config.BaseDep
	Pool    *gorm.DB
}

type FlightPersister interface {
	FindFlights() ([]flightModel.FlightLite, error)
}

func NewFlightPersister(r FlightRepository) FlightPersister {
	return &r
}

func (r *FlightRepository) FindFlights() ([]flightModel.FlightLite, error) {
	var flights []flightModel.FlightLite
	err := r.Pool.Model(&flightModel.FlightLite{}).
		Select(
			"flights.id", "flights.code", "flights.plane_id", "flights.origin_id",
			"origin.name origin_name", "origin.code origin_code", "flights.destination_id",
			"destination.name destination_name", "destination.code destination_code",
			"flights.available_seats", "flights.on_hand_seats", "flights.reserved_seats",
			"flights.price", "flights.depart_at", "flights.duration",
		).
		Joins("JOIN airports origin on origin.id = flights.origin_id").
		Joins("JOIN airports destination on destination.id = flights.destination_id").
		Limit(10).
		Order("id desc").
		Scan(&flights).
		Error

	if err != nil {
		r.BaseDep.Logger.Error("failed to fetch flights", zap.Error(err))
		return flights, err
	}

	return flights, nil
}
