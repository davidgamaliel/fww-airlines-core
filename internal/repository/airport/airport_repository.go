package airport

import (
	"fmt"
	"sync"

	"github.com/fww/core/config"
	model "github.com/fww/core/internal/model/airport"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

type AirportRepository struct {
	BaseDep *config.BaseDep
	Pool    *gorm.DB
}

type AirportPersister interface {
	FindAirports(req model.FindAirportsRequest) ([]model.Airport, int64, error)
}

func NewAirportPersister(r AirportRepository) AirportPersister {
	return &r
}

func (r *AirportRepository) FindAirports(req model.FindAirportsRequest) ([]model.Airport, int64, error) {
	var airports []model.Airport
	var total int64
	var err error
	var wg sync.WaitGroup

	wg.Add(2)
	go func() {
		query := r.Pool.Model(model.Airport{})
		if req.IDLT > 0 {
			query.Where("id < ?", req.IDLT)
		}

		err = query.
			Find(&airports, fmt.Sprintf("slug::text like '%%%s%%'", req.Slug)).
			Limit(req.Limit).
			Order("id desc").Error
		wg.Done()
	}()

	go func() {
		total = r.CountAirports(req)
		wg.Done()
	}()

	wg.Wait()

	if err != nil {
		r.BaseDep.Logger.Error("failed to fetch airports", zap.Error(err))
		return airports, total, err
	}

	return airports, total, nil
}

func (r *AirportRepository) CountAirports(req model.FindAirportsRequest) int64 {
	var total int64
	err := r.Pool.Model(model.Airport{}).
		Where(fmt.Sprintf("slug::text like '%%%s%%'", req.Slug)).
		Count(&total).Error

	if err != nil {
		r.BaseDep.Logger.Error("failed to count airports", zap.Error(err))
	}

	return total
}
