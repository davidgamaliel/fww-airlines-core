package httptransport

import (
	"os"

	"github.com/fww/core/config"
	airportRepo "github.com/fww/core/internal/repository/airport"
	flightRepo "github.com/fww/core/internal/repository/flight"
	userRepo "github.com/fww/core/internal/repository/user"
	"github.com/fww/core/internal/transport/http/airport"
	"github.com/fww/core/internal/transport/http/flight"
	"github.com/fww/core/internal/transport/http/user"
	airportUsecase "github.com/fww/core/internal/usecase/airport"
	flightUsecase "github.com/fww/core/internal/usecase/flight"
	userUsecase "github.com/fww/core/internal/usecase/user"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

type HTTPTransport struct {
	airport        airport.AirportHandler
	user           user.UserHandler
	flight         flight.FlightHandler
	authMiddleware fiber.Handler
}

func NewHTTPTransport() *HTTPTransport {
	return new(HTTPTransport)
}

func (s *HTTPTransport) RegisterUser(base config.BaseDep, pool *gorm.DB) {
	repo := userRepo.NewUserPersister(userRepo.UserRepository{
		BaseDep: &base,
		Pool:    pool,
	})

	usecase := userUsecase.NewUserExecutor(userUsecase.UserUsecase{
		BaseDep:    &base,
		Repository: repo,
		Secret:     os.Getenv("ACCOUNT_SECRET"),
	})
	s.user = user.NewUserHandler(user.User{
		BaseDep: &base,
		Usecase: usecase,
	})
}

func (s *HTTPTransport) RegisterAirport(base config.BaseDep, pool *gorm.DB) {
	repo := airportRepo.NewAirportPersister(airportRepo.AirportRepository{
		BaseDep: &base,
		Pool:    pool,
	})

	usecase := airportUsecase.NewAirportExecutor(airportUsecase.AirportUsecase{
		BaseDep:    &base,
		Repository: repo,
	})
	s.airport = airport.NewAirportHandler(airport.Airport{
		BaseDep: &base,
		Usecase: usecase,
	})
}

func (s *HTTPTransport) RegisterFlight(base config.BaseDep, pool *gorm.DB) {
	repo := flightRepo.FlightPersister(&flightRepo.FlightRepository{
		BaseDep: &base,
		Pool:    pool,
	})

	usecase := flightUsecase.NewFlightExecutor(flightUsecase.FlightUsecase{
		BaseDep:    &base,
		Repository: repo,
	})
	s.flight = flight.NewFlightHandler(flight.Flight{
		BaseDep: &base,
		Usecase: usecase,
	})
}

func (s *HTTPTransport) RegisterAuthMiddleware(gf fiber.Handler) {
	s.authMiddleware = gf
}

// InitRoutes :nodoc:
func (s *HTTPTransport) InitRoutes(route *fiber.App) {
	apiV1 := route.Group("/api/v1")

	accounts := apiV1.Group("/accounts")
	accounts.Post("/login", s.user.Login)

	airport := apiV1.Group("/airports")
	airport.Use(s.authMiddleware)
	airport.Get("", s.airport.FindAirports)

	flight := apiV1.Group("/flights")
	flight.Use(s.authMiddleware)
	flight.Get("", s.flight.FindFlights)
}
