package airport

import (
	"fmt"
	"net/http"

	"github.com/fww/core/config"
	"github.com/fww/core/internal/helper"
	baseModel "github.com/fww/core/internal/model"
	model "github.com/fww/core/internal/model/airport"
	usecase "github.com/fww/core/internal/usecase/airport"
	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
)

type Airport struct {
	BaseDep *config.BaseDep
	Usecase usecase.AirportExecutor
}

type AirportHandler interface {
	FindAirports(c *fiber.Ctx) error
}

func NewAirportHandler(r Airport) AirportHandler {
	return &r
}

func (h *Airport) FindAirports(c *fiber.Ctx) error {
	identity := helper.GetIdentity(c)
	fmt.Println(identity)
	var req model.FindAirportsRequest

	if err := c.QueryParser(&req); err != nil {
		h.BaseDep.Logger.Error("failed to parse query", zap.Error(err))
		return c.Status(http.StatusInternalServerError).
			JSON(err)
	}

	flights, total, err := h.Usecase.FindAirports(req)
	if err != nil {
		return c.Status(http.StatusBadRequest).
			JSON(helper.BusinessErrorResponse(err))
	}
	return c.Status(http.StatusOK).
		JSON(baseModel.PaginationResponse{
			Data: flights,
			Meta: baseModel.PaginationMeta{
				Total: int(total),
				Limit: req.Limit,
				Meta: baseModel.Meta{
					Code:    helper.SUCCESS_RESPONSE_CODE,
					Message: helper.SUCCESS_RESPONSE_MSG,
				},
			},
		})
}
