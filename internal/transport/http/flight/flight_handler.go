package flight

import (
	"fmt"
	"net/http"

	"github.com/fww/core/config"
	"github.com/fww/core/internal/helper"
	baseModel "github.com/fww/core/internal/model"
	usecase "github.com/fww/core/internal/usecase/flight"
	"github.com/gofiber/fiber/v2"
)

type Flight struct {
	BaseDep *config.BaseDep
	Usecase usecase.FlightExecutor
}

type FlightHandler interface {
	FindFlights(c *fiber.Ctx) error
}

func NewFlightHandler(r Flight) FlightHandler {
	return &r
}

func (h *Flight) FindFlights(c *fiber.Ctx) error {
	identity := helper.GetIdentity(c)
	fmt.Println(identity)
	// var req model.FindFlightsRequest

	// if err := c.QueryParser(&req); err != nil {
	// 	h.BaseDep.Logger.Error("failed to parse query", zap.Error(err))
	// 	return c.Status(http.StatusInternalServerError).
	// 		JSON(err)
	// }

	flights, total, err := h.Usecase.FindFlights()
	if err != nil {
		return c.Status(http.StatusBadRequest).
			JSON(helper.BusinessErrorResponse(err))
	}
	return c.Status(http.StatusOK).
		JSON(baseModel.PaginationResponse{
			Data: flights,
			Meta: baseModel.PaginationMeta{
				Total: int(total),
				Limit: 0,
				Meta: baseModel.Meta{
					Code:    helper.SUCCESS_RESPONSE_CODE,
					Message: helper.SUCCESS_RESPONSE_MSG,
				},
			},
		})
}
