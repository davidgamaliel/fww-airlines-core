package user

import (
	"net/http"

	"github.com/fww/core/config"
	"github.com/fww/core/internal/helper"
	"github.com/fww/core/internal/model"
	userModel "github.com/fww/core/internal/model/user"
	userUsecase "github.com/fww/core/internal/usecase/user"
	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
)

type User struct {
	BaseDep *config.BaseDep
	Usecase userUsecase.UserExecutor
}

type UserHandler interface {
	Login(c *fiber.Ctx) error
}

func NewUserHandler(h User) UserHandler {
	return &h
}

func (h *User) Login(c *fiber.Ctx) error {
	var req userModel.UserCredential
	if err := c.QueryParser(&req); err != nil {
		h.BaseDep.Logger.Error("failed to parse query", zap.Error(err))
		return c.Status(http.StatusInternalServerError).
			JSON(err)
	}

	token, e := h.Usecase.Login(req)
	if e != nil {
		return c.Status(http.StatusBadRequest).
			JSON(helper.BusinessErrorResponse(e))
	}

	return c.Status(http.StatusOK).
		JSON(model.Response{
			Data: token,
			Meta: model.Meta{
				Code:    helper.SUCCESS_RESPONSE_CODE,
				Message: helper.SUCCESS_RESPONSE_MSG,
			},
		})
}
