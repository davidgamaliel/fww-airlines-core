// Code generated by MockGen. DO NOT EDIT.
// Source: internal/repository/airport/airport_repository.go

// Package mock_airport is a generated GoMock package.
package mock_repository

import (
	reflect "reflect"

	airport "github.com/fww/core/internal/model/airport"
	gomock "github.com/golang/mock/gomock"
)

// MockAirportPersister is a mock of AirportPersister interface.
type MockAirportPersister struct {
	ctrl     *gomock.Controller
	recorder *MockAirportPersisterMockRecorder
}

// MockAirportPersisterMockRecorder is the mock recorder for MockAirportPersister.
type MockAirportPersisterMockRecorder struct {
	mock *MockAirportPersister
}

// NewMockAirportPersister creates a new mock instance.
func NewMockAirportPersister(ctrl *gomock.Controller) *MockAirportPersister {
	mock := &MockAirportPersister{ctrl: ctrl}
	mock.recorder = &MockAirportPersisterMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockAirportPersister) EXPECT() *MockAirportPersisterMockRecorder {
	return m.recorder
}

// FindAirports mocks base method.
func (m *MockAirportPersister) FindAirports(req airport.FindAirportsRequest) ([]airport.Airport, int64, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "FindAirports", req)
	ret0, _ := ret[0].([]airport.Airport)
	ret1, _ := ret[1].(int64)
	ret2, _ := ret[2].(error)
	return ret0, ret1, ret2
}

// FindAirports indicates an expected call of FindAirports.
func (mr *MockAirportPersisterMockRecorder) FindAirports(req interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "FindAirports", reflect.TypeOf((*MockAirportPersister)(nil).FindAirports), req)
}
