- [ ] 


available = 200
on_hand = 200
reserved =0



create reservation (1)
(+) reserved    1 
(-) available   199
( ) on_hand     200


cancel/reject/failed
(-) reserved    0
(+) available   200
( ) on_hand     200

paid
(-) reserved    0
(-) available   199
(-) on_hand     199


# Success
user -> reservation (book service) -> allocate (core service)
-> process engine (bpm) -> user paid -> success payment (book service)
-> settle (core service)

# cancel/reject/failed
user -> reservation (book service) -> allocate (core service)
-> process engine (bpm) -> failed state (book service) 
-> reject (core service)