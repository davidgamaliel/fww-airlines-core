## Background
- FWW Airline, flight enterprise
- run for more 20 years
- more than **100k transactions** per month
- 24x7 booking operations from all channel
- partner with travel platform(s)
- using AS400 as old system
- design system with cloud compatibility and mobile and web platform

![highlevel](diagrams/highlevel_architecture.drawio.png)
  
## delivarables
- use any free git cloud solution
- sourcecodes
- high level architecture
- ERD
- assumptions
  
## requirements
concetrate on 2 mait functions:

### Core Flight Management System
- contain data and service for flight itself
- hold information about
  - Airport
  - Flight
  - Reservation
  - Passenger
  - Plane
  - Seat
  - Payment
  - Baggage
  - Others (if necessary)
- API shouldn't contain any business logic and rules
- only customize  the DDL, DML and query modification
- rules:
  - 1 seat at 1 flight only booked once
  - 1 plane cannot contain more 172 passengers each flight
  
### Booking Engine Integration System
- all needed channels
  - Mobile Apps -> connect to their own mobile apps
  - Web Apps -> connect to their own web apps
  - API Apps -> partners using FFW service (OpenApi)
- ticket purchase rules:
  - cannot book from the day vefore today
  - 1 user can only book 1 flight per day
  - 1 user can only book 1 seat each flight
  - booking code valid unti a day before flight
  - ticket must be paid 6 hours after bought
  - reservation code must be redeemed 1 hour before flight max
  - ticket should be submitted while onboard
  - workflows
  ![workflows](images/ticket-purchasing-workflows.png)
- payment using 3rd party service
- integration api consumed by:
  - mobile apps
  - web apps
  - 3rd party service
- FWW users must register first to use FWW service

### Assumptions
This iteration only catter for:
- Domestic flight
- Pricing in Rupiahs only
- 