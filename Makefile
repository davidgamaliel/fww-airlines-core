tidy:
	go mod tidy

mod-reset:
	rm -rf ./go.sum && go clean --modcache && go mod tidy

run:
	go run cmd/http/api.go

gen-mock-source:
	mockgen -destination=$(destination) -source=${source}

check-cognitive-complexity:
	find . -type f -name '*.go' -not -name "mock*.go" \
      -exec gocognit -over 15 {} \;

lint: check-cognitive-complexity
	golangci-lint run --print-issued-lines=false --exclude-use-default=false --enable gosec --enable goimports  --enable unconvert --concurrency=2 

unit-test-coverage:
			go test -v -covermode=count ./... -coverprofile=coverage.cov
			go tool cover -func=coverage.cov
