package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/fww/core/config"
	"github.com/fww/core/internal/middleware"
	httptransport "github.com/fww/core/internal/transport/http"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/pprof"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/joho/godotenv"
)

func main() {
	loadEnv()
	baseDep := config.NewBaseDep("fww-core")
	config.InitializeDBPoolConn()

	app := fiber.New()
	app.Use(recover.New())
	app.Use(cors.New())
	app.Use(pprof.New())
	app.Use(logger.New(logger.Config{
		Format: "[${time}] ${status} - ${latency} ${method} ${path}\n",
		// Format:       `${time} {"router_activity" : [${status},"${latency}","${method}","${path}"], "query_param":${queryParams}, "body_param":${body}}` + "\n",
		TimeInterval: time.Millisecond,
		TimeFormat:   "02-01-2006 15:04:05",
		TimeZone:     "Indonesia/Jakarta",
	}))

	//=== healthz route
	app.Get("/", Healthz)
	app.Get("/healthz", Healthz)

	transport := httptransport.NewHTTPTransport()
	transport.RegisterAuthMiddleware(middleware.AuthSecurity(os.Getenv("ACCOUNTS_SECRET")))
	transport.RegisterUser(*baseDep, config.Pool)
	transport.RegisterAirport(*baseDep, config.Pool)
	transport.RegisterFlight(*baseDep, config.Pool)
	transport.InitRoutes(app)

	if err := app.Listen(fmt.Sprintf(":%s", os.Getenv("APP_PORT"))); err != nil {
		log.Fatal(err)
	}
}

func loadEnv() {
	_, err := os.Stat(".env")
	if err == nil {
		err = godotenv.Load()
		if err != nil {
			panic("failed load env file")
		}
	}
}

func Healthz(c *fiber.Ctx) error {
	res := map[string]interface{}{
		"data": "Service is up and running",
	}

	if err := c.JSON(res); err != nil {
		return err
	}

	return nil
}
