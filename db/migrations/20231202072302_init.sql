-- migrate:up
create table if not exists users (
    id bigserial primary key,
    name varchar(200),
    email varchar(150) not null,
    phone_no varchar(20),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);

create table if not exists planes (
    id bigserial primary key,
    name varchar(150) not null,
    code varchar(15) not null,
    seat_capacity int not null default 0
);

create table if not exists seats (
    plane_id bigint not null ,
    seat_no varchar(8) not null,
    primary key(plane_id, seat_no),
    constraint fk_plane foreign key (plane_id) references planes(id)
);

create table if not exists airports (
    id bigserial primary key,
    name varchar(250) not null,
    code varchar(5) not null,
    country varchar(50) not null,
    province varchar(50) not null,
    city varchar(50) not null,
    district varchar(50) not null,
    area varchar(50) not null,
    slug jsonb null
);

create table if not exists flights (
    id bigserial primary key,
    code varchar(10) not null,
    plane_id bigint not null,
    origin bigint not null,
    destination bigint not null,
    available_seats int,
    on_hand_seats int,
    reserved_seats int,
    price float8,
    depart_at timestamp without time zone,
    duration int,
    slug jsonb null,
    constraint fk_plane foreign key (plane_id) references planes(id),
    constraint fk_origin foreign key (origin) references airports(id),
    constraint fk_destination foreign key (destination) references airports(id)
);


-- migrate:down
drop table if exists flights;
drop table if exists airports;
drop table if exists seats;
drop table if exists planes;
drop table if exists users;